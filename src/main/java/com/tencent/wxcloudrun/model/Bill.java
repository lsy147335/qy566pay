package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Bill implements Serializable {

  public String phone;
  public String payUrl;
}
