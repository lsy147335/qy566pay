package com.tencent.wxcloudrun.dto;

import java.util.Date;

public class Content {

    public String ToUserName;
    public String FromUserName;
    public Date CreateTime;
    public String MsgType;
    public String Content;
    public Long MsgId;
    public Integer bizmsgmenuid;
}
