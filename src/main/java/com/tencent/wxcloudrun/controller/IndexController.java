package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.dao.BillMapper;
import com.tencent.wxcloudrun.dto.Content;
import com.tencent.wxcloudrun.model.Bill;
import com.tencent.wxcloudrun.service.CounterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * index控制器
 */
@RestController
public class IndexController {
    final Logger logger;
    @Resource
    BillMapper billMapper;

    public IndexController(@Autowired CounterService counterService) {
        this.logger = LoggerFactory.getLogger(IndexController.class);
    }

    /**
     * 主页页面
     *
     * @return API response html
     */
    @GetMapping
    public String index() {
        return "index";
    }

    @PostMapping(value = "/content")
    Object get(@RequestBody Object object) {
        /*{
        "ToUserName": "gh_cb93ab5e0282",
            "FromUserName": "ooSDq5mG3YBegKsB2WxDamNXmIe0",
            "CreateTime": 1701071436,
            "MsgType": "text",
            "Content": "xxx",
            "MsgId": 24353489328207289,
            "bizmsgmenuid": 0
    }* */
        logger.info(JSONObject.toJSONString(object));
        Content content = JSONObject.parseObject(JSONObject.toJSONString(object), Content.class);
        Long createTime = new Date().getTime() / 1000;
        Map<String, Object> result = new HashMap<>();
        result.put("ToUserName", content.FromUserName);
        result.put("FromUserName", content.ToUserName);
        result.put("CreateTime", createTime);
        result.put("MsgType", content.MsgType);
        String mobileRegEx = "^1[3,4,5,6,7,8,9][0-9]{9}$";

        Pattern pattern = Pattern.compile(mobileRegEx);
        Matcher matcher = pattern.matcher(content.Content);

        if (!matcher.matches()) {
            content.Content = "<a href=\"weixin://bizmsgmenu?msgmenucontent="+content.Content+"&msgmenuid=xxx标签\">请在聊天框内输入正确的手机格式</a>";
            result.put("Content", content.Content);
            logger.info("result:{}", JSONObject.toJSONString(result));
            return JSONObject.toJSONString(result);
        }

        Bill byId = billMapper.getById(content.Content);
        if (byId == null) {
            content.Content = "<a href=\"weixin://bizmsgmenu?msgmenucontent="+content.Content+"&msgmenuid=xxx标签\">还未抢到订单,手机号无误请继续点击我</a>";
        }else {
            content.Content = byId.payUrl;
        }
        result.put("Content", content.Content);
        logger.info("result:{}", JSONObject.toJSONString(result));
        return JSONObject.toJSONString(result);
    }

    @PostMapping("/save")
    public Integer save(@RequestBody Bill bill) {
       return billMapper.save(bill);
    }
}
