package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.Bill;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BillMapper {

  Bill getById(@Param("phone") String phone);
  int save(Bill bill);
}
